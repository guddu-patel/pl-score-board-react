import React from "react";
import ReactDom from "react-dom";
import {Home} from "./app/home/home"
import {Navbar} from "./app/navbar/navbar"
let comp=(
    <div>
        <Navbar ></Navbar>
        <div className="container">
            <Home/>
        </div>
    </div>
)
ReactDom.render(comp,document.getElementById('root'));