import React from "react"
import "./home.css";
import {Matchname} from "./../matchName/matchname"
import {Detailmodal} from "./../detailmodal/detailmodal"
export class Home extends React.Component {
   
    constructor(props) {
        super(props);     
        this.state = {
          session:"2015-16",
          sort:'desc',
          data:[],
          clubs:null,
          original:[],
          name:null,
          selectedteam:{}
        };
        this.handleChange = this.handleChange.bind(this);
        this.filterData = this.filterData.bind(this);
        this.sortData = this.sortData.bind(this);
        this.border = this.border.bind(this);
        this.controller = new AbortController();
        this.searchRef = React.createRef();
    }
    handleChange(e) {
      console.log("Session Selected!!");
      let tempVal=e.target.value;
      this.setState({ session: tempVal },()=>{

            this.getData(this.state.session);
      });
    }
    componentDidMount() {
        this.getClubs(this.state.session);
      }
     
    render() {
      return this.getTable();
    }
    getClubs(session){
        const apiUrl = `https://raw.githubusercontent.com/openfootball/football.json/master/${session}/en.1.clubs.json`;
        if(!this.state.clubs)
        fetch(apiUrl,{signal:this.controller.signal})
            .then((response) => response.json() )
            .then((data) => {
                    this.setState({clubs:data.clubs});
                    console.log('This is your club data', data);
                    this.getData(session);
            });
            else
                this.getData(session)
    }
   getData(session){
    showLoader();
    this.searchRef.current.value = '';
    const apiUrl = `https://raw.githubusercontent.com/openfootball/football.json/master/${session}/en.1.json`;

    fetch(apiUrl,{signal:this.controller.signal})
          .then((response) => response.json() )
          .then((data) => {
                this.setState({name:data.name});
                this.formatData(data.rounds||data.matches);
                console.log('This is your match data', data);
                hideLoader();
        });
  }
  formatData(data){
      let temp=[];
      data.map((d)=>{
        d.matches.map((m)=>{
            if(temp[m.team1]){
                temp[m.team1].gf+=m.score.ft[0];
                temp[m.team1].ga+=m.score.ft[1];
                if(m.score.ft[0]>m.score.ft[1]) {temp[m.team1].w++;temp[m.team1].p+=3;temp[m.team1].m.push('w');};
                if(m.score.ft[0]<m.score.ft[1]) {temp[m.team1].l++;temp[m.team1].p+=0;temp[m.team1].m.push('l');};
                if(m.score.ft[0]==m.score.ft[1]) {temp[m.team1].d++;temp[m.team1].p+=1;temp[m.team1].m.push('d');};
            }else{
                temp[m.team1]={name:m.team1,w:0,l:0,gf:m.score.ft[0],ga:m.score.ft[1],d:0,p:0,m:[]}
                if(m.score.ft[0]>m.score.ft[1]) {temp[m.team1].w=1;temp[m.team1].p=3;temp[m.team1].m.push('w');};
                if(m.score.ft[0]<m.score.ft[1]) {temp[m.team1].l=1;temp[m.team1].p=0;temp[m.team1].m.push('l')};
                if(m.score.ft[0]==m.score.ft[1]) {temp[m.team1].d=1;temp[m.team1].p=1;temp[m.team1].m.push('d')};
            }
            if(temp[m.team2]){
                temp[m.team1].gf+=m.score.ft[0];
                temp[m.team1].ga+=m.score.ft[1];
                if(m.score.ft[0]>m.score.ft[1]) {temp[m.team2].w++;temp[m.team2].p+=3;temp[m.team2].m.push('w')};
                if(m.score.ft[0]<m.score.ft[1]) {temp[m.team2].l++;temp[m.team2].p+=0;temp[m.team2].m.push('l')};
                if(m.score.ft[0]==m.score.ft[1]) {temp[m.team2].d++;temp[m.team2].p+=1;temp[m.team2].m.push('d')};
            }else{
                temp[m.team2]={name:m.team2,w:0,l:0,gf:m.score.ft[0],ga:m.score.ft[1],d:0,m:[]}
                if(m.score.ft[0]>m.score.ft[1]) {temp[m.team2].w=1;temp[m.team2].p=3;temp[m.team2].m.push('w')};
                if(m.score.ft[0]<m.score.ft[1]) {temp[m.team2].l=1;temp[m.team2].p=0;temp[m.team2].m.push('l')};
                if(m.score.ft[0]==m.score.ft[1]) {temp[m.team2].d=1;temp[m.team2].p=1;temp[m.team2].m.push('d')};
            }
        })
      });
      let t=[];
      for(let key in temp){
        let club=null;
        if(this.state.clubs)
        club = this.state.clubs.filter((data)=>{
            return data.name.toLowerCase()== key.toLowerCase();
        })
        t.push({name:key,data:temp[key],c:club[0]})
      }
      this.setState({data:t,original:t,sort:'desc'},()=>{
         this.sortData(); 
      });
      
  }
  sortData(){
    let sortable=null;
    if(this.state.sort=='asc'){
        sortable=this.state.data.sort(function(a, b){
            return a.data.p - b.data.p;
        })
        
    }
    if(this.state.sort=='desc'){
        sortable=this.state.data.sort(function(a, b){
            return b.data.p - a.data.p;
        })
        sortable=this.markBorder(sortable);
    }
    console.log("final sorted Data",sortable);
    let ts=this.state.sort=='asc'?'desc':'asc';
    this.setState({data:sortable,sort:ts});
  }
  selectTeam(data){
    this.setState({selectedteam:data});
  }
  filterData(e) {
    console.log("filter text");
    let tempVal=e.target.value;
    if(!tempVal){
        this.setState({data:this.state.original});
        return;
    }    
    let od=this.state.original.filter((data)=>{
        return data.name.toLowerCase().includes(tempVal.toLowerCase());
    })
    this.setState({data:od});
  }
  markBorder(data){
      data.forEach((e,i) => {
        // i<3?e.b='redbd':i>this.state.data.length-5?e.b='greenbd':''
        i<4?e.b='greenbd':i>this.state.data.length-4?e.b='redbd':''
      });
      this.setState({original:data});
      return data;
  }
  componentWillUnmount(){
    setTimeout(() => {

          this.controller.abort();
    },100)
  }
  border(i){
    if(this.state.sort=='asc')
        return i<4?'greenbd':i>this.state.data.length-4?'redbd':''
    if(this.state.sort=='desc')
        return i<3?'redbd':i>this.state.data.length-5?'greenbd':''
  
  }
  
  getTable(){
     const dates=["2013-14","2014-15","2015-16","2016-17","2017-18"];
     if(!this.state.data)
            return null;

     return <div>
                <div className="row">
                <div className="input-group my-3 col-6">
                    <div className="input-group-prepend">
                        <span className="input-group-text">Session</span>
                    </div>
                    <select className="form-control" value={this.state.session} onChange={this.handleChange}>
                    {dates.map((date,i) => (
                        <option value={date} key={i}>{date}</option>
                    ))}
                    </select>
                </div>
                <div className="input-group my-3 col-6">
                    <div className="input-group-prepend">
                        <span className="input-group-text">Team name</span>
                    </div>
                    <input type="text" className="form-control" ref={this.searchRef} onChange={this.filterData} placeholder="Search my team..."/>
                   
                </div>
                </div>
                <Matchname matchname={this.state.name} />

                <table className="table">
                    <thead>
                    <tr>
                        <th>SN</th>
                        <th>Team name</th>
                        <th>MP</th>
                        <th>Win</th>
                        <th>Draw</th>
                        <th>Lose</th>
                        <th>GF</th>
                        <th>GA</th>
                        <th>GD</th>
                        <th onClick={this.sortData}>Point</th>
                        <th>Last matches</th>
                    </tr>
                    </thead>
                    <tbody>
                        {this.state.data.map((item,i)=>{
                            let value=item.data;
                        
                            return <tr key={i}>
                                <td className={item.b}>{i+1}</td>
                                <td data-toggle="modal" data-target="#myModal" onClick={e=>this.selectTeam(item)}>{item.name}</td>
                                <td>{value.m.length}</td>
                                <td>{value.w}</td>
                                <td>{value.d}</td>
                                <td>{value.l}</td>
                                <td>{value.gf}</td>
                                <td>{value.ga}</td>
                                <td>{value.gf-value.ga}</td>
                                <td>{value.p}</td>
                                <td>
                                {value.m.slice(value.m.length-5).map((ms,j)=>{
                                    if(ms=='w')
                                    return <span className="badge badge-pill badge-success badgeWidth"  key={j}>{ms}</span> 
                                    if(ms=='l')
                                    return <span className="badge badge-pill badge-danger badgeWidth" key={j}>{ms}</span> 
                                    if(ms=='d')
                                    return <span className="badge badge-pill badge-secondary badgeWidth" key={j}>{ms}</span>
                                })}
                                </td>

                            </tr>
                        })}
                
                
                    </tbody>
                </table>
                
               <Detailmodal data={this.state.selectedteam}/>
            </div>
  }
}