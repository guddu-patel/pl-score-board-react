import React from "react";

export class Matchname extends React.Component{
    constructor(props){
        super(props);
    }
    render(){
        return <h1>{this.props.matchname}</h1>
    }
}