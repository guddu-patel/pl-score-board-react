import React, { Component,useState } from "react";
import { Modal, Button } from "react-bootstrap";

export class Detailmodal extends React.Component {
    constructor(props) {
        super(props);     
        this.state = {
          show:false

        };
        this.handleClose = this.handleClose.bind(this);
        this.handleShow = this.handleShow.bind(this);
        
    }
    handleClose(){
        this.setState({show:false});
    }
    handleShow(){
        this.setState({show:true});
    }
  render() {
    return (<>
        <div className="modal" id="myModal">
            <div className="modal-dialog modal-lg">
                <div className="modal-content">

                <div className="modal-header">
                    <h4 className="modal-title">{this.props.data.name}</h4>
                    <button type="button" className="close" data-dismiss="modal">&times;</button>
                </div>

                <div className="modal-body">
                    <div className="card" >
                        <img className="card-img-top"style={{width:"auto",height:'200px'}} src="https://static.toiimg.com/photo/75061606.cms" alt="Card image cap" />
                        <div className="card-body">
                            <h5 className="card-title">{this.props.data.c?.name}</h5>
                            <p className="card-text">Country: <b>{this.props.data.c?.country}</b>   </p>
                            <p className="card-text">Code: <b>{this.props.data.c?.code}</b>   </p>
                            <a href="#" className="btn btn-primary">Go somewhere</a>
                        </div>
                    </div>
                <div>    
                     <br/>           
                    <a className="twitter-timeline" data-height="400" data-theme="dark" href="https://twitter.com/ManUtd">Tweets by ManUtd</a> 
                </div>
                </div>

                <div className="modal-footer">
                    <button type="button" className="btn btn-danger" data-dismiss="modal">Close</button>
                </div>

                </div>
            </div>
        </div>
    </>
    );
  }
}