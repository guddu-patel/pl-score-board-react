## Premier League score card

#### Example:
Premier League [click](https://www.premierleague.com/tables)

An application used to Display data form github based on user session selection of match, built with React, JavaScript,HTML and CSS.

## Project Status
It just a small module which display score points in detail (completed)

Demo [click](http://plscore.guddu.com.np/)

## Installation and Setup Instructions

#### Example:  

Clone down this repository. You will need `node` and `npm` installed globally on your machine.  

Installation:

`npm install`  



To Start Server:

`npm start`  

To Visit App:

`localhost:8080`  

Note: Please be sure your internet is working to run the project properly.

## Developer
Guddu kumar [click](guddu.com.np)

Guddu.patel13@gmail.com
## Credits
Open football [click](https://github.com/openfootball)

## License
Its free licence so use can use this snippent anywhere(MIT)

MIT © [Guddu kumar](guddu.com.np)